/* Takes as input number of vertices and gridSize.
Intialises empty adjacency matrix of correct size (using adjMat function), and randomises coordinates of vertices.
Also ensures no 3 vertices are on the same line (by checking slope) against every pair).*/

randomGeneratorNode = (num) => {
    let coordX = new Array(num).fill(-1)
    let coordY = new Array(num).fill(-1)

    for (var i = 0 ; i < num ; ){
        coordX[i] = Math.floor((Math.random() * 15) + 1); 
        coordY[i] = Math.floor((Math.random() * 15) + 1); 
        let chk=0
        for (var j = 0; j < i; j++){
            if (coordX[i]!=coordX[j] && coordY[i]!=coordY[j]){
                let chk2 = 0 
                for (let k = 0; k < j; k++  ){
                if (((coordX[i]-coordX[j])/(coordY[i]-coordY[j]))!=((coordX[i]-coordX[k])/(coordY[i]-coordY[k]))){    
                chk2++;
                }
                }
                if (chk2==j){
                chk++
                }
            }
        }
        if (chk==i){
            
            makeGraph(coordX[i], coordY[i])
            i++
            }
        }
        let coord = new Array()
        coord.push(coordX)
        coord.push(coordY)
        return coord
}




/* Takes as input number of vertices, existing matrix, and boolean for directed/undirected graph.
Randomises number of edges, source vertex and target vertex, and adds edges (using addEdgeMat function). */
randomGeneratorEdge = (num, coord, isDirected) => {
    let mat = adjMat(num)
    if (isDirected==1){
        numEdges = Math.floor((Math.random() * (num*(num-1)/6)) );
        for (let i = 0; i <= numEdges; ){
            let source = Math.floor((Math.random() * num ) )
            let target = Math.floor((Math.random() * num ) )
            if (source != target && mat[source][target] == 0 && mat[target][source]==0){ 
                addEdgeMat(source, target, isDirected, mat)
                drawDirEdge(parseInt(coord[0][source]), parseInt(coord[1][source]), parseInt(coord[0][target]), parseInt(coord[1][target]))
                i++;
                }
            }
        }
    else {
        numEdges = Math.floor((Math.random() * (num*(num-1)/2)) );
        for (let i = 0; i <= numEdges; ){
            let source = Math.floor((Math.random() * num ))
            let target = Math.floor((Math.random() * num ))
            if (source != target && mat[source][target] == 0 && mat[target][source]==0){
                addEdgeMat(source, target, isDirected, mat)
                drawUnDirEdge(coord[0][source], coord[1][source], coord[0][target], coord[1][target])
                i++;
                }
            }
        }
    console.log(coord)
    console.log(mat) 
    return mat
    }

/* Takes as input number of vertices, existing matrix, boolean for directed/undirected graph and maximum number of edges.
Randomises number of edges, source vertex and target vertex, and adds edges (using addEdgeMat function). */
randomGeneratorSparseEdge = (num, coord, isDirected, maxEdges) => {
    let mat = adjMat(num)
    if (isDirected==1){
        numEdges = Math.floor(Math.random() * maxEdges );
        for (let i = 0; i <= numEdges; ){
            let source = Math.floor((Math.random() * num ) )
            let target = Math.floor((Math.random() * num ) )
            if (source != target && mat[source][target] == 0 && mat[target][source]==0){ 
                addEdgeMat(source, target, isDirected, mat)
                drawDirEdge(parseInt(coord[0][source]), parseInt(coord[1][source]), parseInt(coord[0][target]), parseInt(coord[1][target]))
                i++;
                }
            }
        }
    else {
        numEdges = Math.floor(Math.random() * maxEdges );
        for (let i = 0; i <= numEdges; ){
            let source = Math.floor((Math.random() * num ))
            let target = Math.floor((Math.random() * num ))
            if (source != target && mat[source][target] == 0 && mat[target][source]==0){
                addEdgeMat(source, target, isDirected, mat)
                drawUnDirEdge(coord[0][source], coord[1][source], coord[0][target], coord[1][target])
                i++;
                }
            }
        }
    console.log(coord)
    console.log(mat) 
    return mat
    }

randomGraphGenerator = (num, isDirected) => {
if (num > 13){
    num = 13
}
let coord = randomGeneratorNode(num)
let mat = randomGeneratorEdge(parseInt(num), coord, isDirected)

}




randomGeneratorForm = () => {
    document.body.innerHTML += `<form name="randomGenerate" onsubmit="randomGraphGenerator(randomGenerate.num.value, randomGenerate.isDir.value); return false">
    Enter number of random nodes:<br>
    <input type="text" name="num">
    <br>
    Enter boolean for directed(1) or undirected(0):<br>
    <input type="text" name="isDir">
    <input type="submit">
</form>`

}