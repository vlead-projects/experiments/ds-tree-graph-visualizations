# GraphLibrary

- Code is yet to be cleaned up and optimised, this is a Beta

## Authored By :-
- Mustafa Siddiqui (https://github.com/mushie101)
- Shubhangi Dutta (https://github.com/celinarose)

## Steps to contribute:
- Every time you work on a new funtion make a new file.
- Use camel casing and write readable code.
- Please do not commit to master, commit from a branch (preferably your name) and create a merge request.
- Be detailed about how your code works and what it does, this will help us create the final README.
- Happy coding! :D

## Steps to use User-Defined Graph Visualisation:

- Import required js files in the head of your HTML file as follows:  
`<script src="Your/Path/GraphLibrary/drawGraph.js"></script>`  
`<script src="Your/Path/GraphLibrary/adjMatrix.js"></script>`  

- Call functions in the body of your HTML file as follows:  
`<script>`  
&nbsp; &nbsp; &nbsp; &nbsp; `dirGraphGenerator();` For user-defined directed graphs      
&nbsp; &nbsp; &nbsp; &nbsp; `unDirGraphGenerator();` For user-defined undirected graphs        
`</script>`  

## Steps to use Random Graph Visualisation:

- Import required js files in the head of your HTML file as follows:  
`<script src="Your/Path/GraphLibrary/drawGraph.js"></script>`  
`<script src="Your/Path/GraphLibrary/randomGen.js"></script>`  
`<script src="Your/Path/GraphLibrary/adjMatrix.js"></script>`  

- Call functions in the body of your HTML file as follows:  
`<script>`  
&nbsp; &nbsp; &nbsp; &nbsp; `randomGeneratorForm()` For random directed or undirected graphs      
       
`</script>`  

## Steps to use Random Directed Acyclic Graph (DAG) Visualisation:

- Import required js files in the head of your HTML file as follows:  
`<script src="Your/Path/GraphLibrary/drawGraph.js"></script>`  
`<script src="Your/Path/GraphLibrary/generateDAG.js"></script>`  
`<script src="Your/Path/GraphLibrary/adjMatrix.js"></script>`  

- Call functions in the body of your HTML file as follows:  
`<script>`  
&nbsp; &nbsp; &nbsp; &nbsp; `DAGForm()` For random directed acyclic graphs      
       
`</script>`  


## Steps to use User-Defined Tree Visualisation:

- Import required js files in the head of your HTML file as follows:  
   <script src="../ds-tree-graph-visualizations/drawTree.js"></script> 

- Call functions in the body of your HTML file as follows:  
`<script>`  
&nbsp; &nbsp; &nbsp; &nbsp; `treeGenerator()` For user defined trees
       
`</script>`  

## Steps to use Random Tree Visualisation:

- Import required js files in the head of your HTML file as follows:  
   <script src="../ds-tree-graph-visualizations/drawTree.js"></script> 
   <script src="../ds-tree-graph-visualizations/randomTree.js"></script> 
   

- Call functions in the body of your HTML file as follows:  
`<script>`  
&nbsp; &nbsp; &nbsp; &nbsp; `randomTreeGenForm()` For random trees
       
`</script>` 