/* Takes as input coordinates of nodes and colour to change to.
Changes node colour. */ 
colourChangeGraphNode = (nodeX, nodeY, colour) => {
let node =  (parseInt(nodeY) - 1) * 15 + parseInt(nodeX)
let graph = document.getElementById(`${node}`)
graph.setAttribute("fill", `${colour}`)
}

/* Takes as input number of the node.
Changes node colour. */ 
colourChangeTreeNode = (node, colour) => {
   let graph = document.getElementById(`${node}`)
    console.log(node)
    graph.setAttribute("fill", `${colour}`)
    }


colourChangeGraphForm = () => {   
    document.body.innerHTML += `<form name="colourChangeGraph" onsubmit="colourChangeGraphNode(colourChangeGraph.x.value, colourChangeGraph.y.value, colourChangeGraph.col.value ); return false">
                                   Enter x coordinate:<br>
                                    <input type="text" name="x">
                                    <br>
                                    Enter y coordinate:<br>
                                    <input type="text" name="y">
                                    <br>
                                    Enter colour:<br>
                                    <input type="text" name="col">
                                    <br>
                                    <input type="submit">
                                </form>`
}

colourChangeTreeForm = () => {   
    document.body.innerHTML += `<form name="colourChangeTree" id="colourChangeTree" onsubmit="colourChangeTreeNode(colourChangeTree.node.value, colourChangeTree.col.value ); return false">
                                   Enter node number:<br>
                                    <input type="text" name="node">
                                    <br>
                                    Enter colour:<br>
                                    <input type="text" name="col">
                                    <br>
                                    <input type="submit">
                                </form>`

    
}