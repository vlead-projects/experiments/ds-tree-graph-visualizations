/* Takes as input the array size and max value of the elements (100 by default)
Randomly generates a array of given size and max values.
Returns array*/

randomArrayGenerator = (num) => {
let maxVal = 100;
let array = new Array();
for (let i = 0; i < num; i++){
    array[i] = Math.floor((Math.random() * maxVal));
}
return array;   
}
randomTreeGenerator = (num) => {
    let array = randomArrayGenerator(num)
    makeTreeFromArray(array)
    console.log(array)
}

randomTreeGenForm = () => {
    document.body.innerHTML += `<form name="randomTreeMaker" onsubmit="randomTreeGenerator(randomTreeMaker.num.value);return false">
    Enter number of nodes:<br>
    <input type="text" name="num"><br>
    <input type="submit">
    </form>`
}
