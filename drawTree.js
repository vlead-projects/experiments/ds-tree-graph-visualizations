let counter = 0
let rand = 0
/* Takes as input array.
Draws tree given array */

makeTreeFromArray = (array) => {
    counter = 0
    rand = 1
    drawTreeNodes()
    makeCanvasTree()
    for(let index = 0; index < array.length; index++, counter++){
        makeTree(array[index])
        drawEdgeTree()
    }
} 

/* Takes as input value of node.
Adds node of given value to tree. */
makeTree = (value) => {
    let tree = document.getElementById(`${counter}`)
    tree.setAttribute("visibility", "visible")
    let text = document.createElementNS("http://www.w3.org/2000/svg","text")
    let innerText = document.createTextNode(`${value}`)
    text.appendChild(innerText)
    text.setAttribute("x","50%")
    text.setAttribute("y","55%")
    text.setAttribute("fill","black")
    text.setAttribute("text-anchor", "middle")
    text.setAttribute("font-size","50%")
    console.log("svg" + `${counter}`)
    let svgBlock = document.getElementById("svg" + `${counter}`)
    svgBlock.appendChild(text)
}




/* Sets up canvas and drawing for tree */
drawTreeNodes = () => {
    let widthOfParent = "50"
    let heightOfParent = "50vh"
    heightOfLayer = "10"
    let container = document.createElement("div")
    container.setAttribute("id","container")
    container.style.width = `50vw`
    container.style.height = `${heightOfParent}px`
    // container.style.display = "inline-flex"
    let arrayIndex = 0;
    let nums = 0;
    document.body.appendChild(container)
    for(let layerNumber = 0;layerNumber <5;layerNumber++) {
        for(let horizontal = 0; horizontal< Math.pow(2,layerNumber);horizontal++) {
            let childDiv = document.createElement("div")
            childDiv.style.height = `10vh`
            childDiv.setAttribute("id", `div${arrayIndex}`)
            let widthOfLayer = widthOfParent / (Math.pow(2, layerNumber))
            childDiv.style.width = `${widthOfLayer}vw`
            container.appendChild(childDiv)
            childDiv.style.position = "absolute"
            childDiv.style.top = `${layerNumber*heightOfLayer}vh`
            childDiv.style.left = `${horizontal*widthOfLayer}vw`
            let svgBlock = document.createElementNS("http://www.w3.org/2000/svg", "svg")
            svgBlock.setAttribute("height", `${heightOfLayer}vh`)
            svgBlock.setAttribute("width", `${heightOfLayer}vh`)
            svgBlock.setAttribute("id",`svg${nums}`);
            nums++;
            svgBlock.style.position = "relative"
            svgBlock.style.left = "50%"
            svgBlock.style.webkitTransform = "translateX(-50%)"
            svgBlock.style.transform = "translateX(-50%);"
            childDiv.appendChild(svgBlock)
            let node = document.createElementNS("http://www.w3.org/2000/svg", "circle")
            node.setAttribute("cx", "50%")
            node.setAttribute("cy", "50%")
            node.setAttribute("visibility", "hidden")
            node.setAttribute("fill", "white")
            node.setAttribute("id",`${arrayIndex++}`)
            node.setAttribute("r","0.675vw")
            node.setAttribute("stroke","#000000")
            node.setAttribute("strokeWidth","3%")
            svgBlock.appendChild(node)
        }
    }
}

/* Function to draw tree edges */
drawEdgeTree = () => {
    let can = document.getElementById("canvas")
    let ctx = can.getContext('2d')
    let bodyRect = document.body.getBoundingClientRect(), canRect = can.getBoundingClientRect()
    let yOffset = canRect.top-bodyRect.top
    let xOffset = canRect.left-bodyRect.left
    document.body.style.padding = "0"
    document.body.style.margin = "0"
    if (counter > 0) {
        let Node2 = document.getElementById(`${counter}`)
        let Node1 = document.getElementById(`${Math.floor((counter - 1) / 2)}`)
        let circle1Left = Node1.getBoundingClientRect().left
        let circle1Top = Node1.getBoundingClientRect().top
        let circle2Left = Node2.getBoundingClientRect().left
        let circle2Top = Node2.getBoundingClientRect().top
        let radius = (Node1.getBoundingClientRect().right - Node1.getBoundingClientRect().left) / 2
        ctx.beginPath();
        ctx.moveTo(circle1Left-xOffset+radius, circle1Top-yOffset+radius);
        ctx.lineTo(circle2Left-xOffset+radius, circle2Top-yOffset+radius);
        ctx.strokeStyle = "black"
        ctx.lineWidth = 1.5
        ctx.stroke()
    }
    if(rand == 0) counter ++
}

/*Resizes canvas for edges every time window is resized*/
reScaleCanvas = () => {
    let canvas = document.getElementById("canvas")
    let height = document.documentElement.clientHeight * 0.5
    let width = document.documentElement.clientWidth * 0.5
    canvas.style.height = `${height}px`
    canvas.style.width = `${width}px`;
}

/* Makes canvas to draw edges. */

makeCanvasTree = () => {
    document.body.style.margin = "0px"
    let container = document.getElementById("container")
    let width = document.documentElement.clientWidth*0.5
    let height = document.documentElement.clientHeight*0.5
    let canvas = document.createElement("CANVAS")
    container.appendChild(canvas)
    canvas.setAttribute("height",`${height}`)
    canvas.setAttribute("width", `${width}`)
    canvas.setAttribute("id","canvas")
}
/* Takes as input value of new node and existing array.
Adds node to array.
Returns array. */

addNodeArray = (value, array) => {
array.push(value)
console.log(array)
return array
}

/* Form and utility function to draw tree */

treeGenerator = () => {
        let array = new Array()
        drawTreeNodes()
        makeCanvasTree()
        var text_input = document.createElement("INPUT");
        text_input.setAttribute("type", "text");
        text_input.setAttribute("id", "text_box");
        var button = document.createElement("button");
        button.innerHTML = "Enter Node: ";
        button.addEventListener ("click", function() {
            addNodeArray(document.getElementById("text_box").value, array)
            makeTree(document.getElementById("text_box").value); drawEdgeTree();
            document.getElementById("text_box").value = "";
        });
        document.body.append(text_input);
        document.body.append(button);
}

window.addEventListener('load', reScaleCanvas, false);
window.addEventListener('resize', reScaleCanvas, false);