# Library for Graph Visualisation

## TODO as of 10th June, 2019
- Arrows should be outside the nodes
- Add backend to UD graphs

## Requirements: 
- Vanilla js

## Functionalities
- Graph Drawing: 
    - Should be able to draw graph acc to user specifications
        - Come up with what all specifications, implement a basic adjacency matrix based inplementation : Done (Shubhangi)
    - Should be able to draw undirected and directed graphs
        - User specifications, come up with a way to draw arrows: Done (Mustafa)
    - Should have straight edges in the graphs: Done (Mustafa)
        - Should be responsive: In progress (Mustafa) 
    - Should have circular nodes: Done (Mustafa)
        - Should be responsive: Done (Shubhangi)
    - Should follow VLEAD's colour standards: Done (Shubhangi)

- Graph Modification
    - Should be able to add and delete nodes, along with linked edges (preferably in a single line)
        - Delete node +  delete edges: Matrix part done (Shubhangi)
    - Should be able to add and delete edges between existing nodes (preferably in a single line)
        - This + update matrix: Matrix part done (Shubhangi)

- Graph Deletion
    - Should be able to remove all nodes at once : Done (Mustafa)

- Graph Generation
    - Should be able to generate a random graph of a given type (Tree, DAG, No restrictions: In Progress (Mustafa), Matrix part done (Shubhangi)
    - Should allow user to specify number of nodes: In Progress (Mustafa), Matrix part done (Shubhangi)
- Tree Drawing
    - Should be able to add node by node in a binary tree: Done (Mustafa)
    - Have a maximum of 31 nodes (5 layers with the same height, different widths): Done (Both)
        - Should be responsive: Done (Shubhangi)
    - Tree should have straight edges: Done (Mustafa)
        - Should be responsive: In progress (Mustafa)
- Tree Generation
    - Should be able to generate a random tree out of an array of a given size: Backend part done (Both)
    