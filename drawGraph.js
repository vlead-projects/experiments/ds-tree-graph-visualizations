drawDirEdge = (node1x, node1y, node2x, node2y) => {
    let can = document.getElementById("can");
    let bodyRect = document.body.getBoundingClientRect(), canRect = can.getBoundingClientRect()
    let node1 = (parseInt(node1y) - 1) * 15 + parseInt(node1x)
    let node2 = (parseInt(node2y) - 1) * 15 + parseInt(node2x)
    let Node1 = document.getElementById(`${node1}`)
    let Node2 = document.getElementById(`${node2}`)
    let circle1Left = Node1.getBoundingClientRect().left
    let circle1Top = Node1.getBoundingClientRect().top
    let circle2Left = Node2.getBoundingClientRect().left
    let circle2Top = Node2.getBoundingClientRect().top
    let radius = (Node1.getBoundingClientRect().right - Node1.getBoundingClientRect().left) / 2
    radius = parseFloat(radius)
    let yOffset = canRect.top-bodyRect.top
    let xOffset = canRect.left-bodyRect.left
    let headlen = 10;
    let angle = Math.atan2((circle2Top-yOffset+radius-radius)-(circle1Top-yOffset+radius),(circle2Left-xOffset+radius)-(circle1Left-xOffset+radius));
    document.body.style.padding = "0"
    document.body.style.margin = "0"
        let ctx = can.getContext("2d");
    ctx.beginPath();
    ctx.globalCompositeOperation='destination-over';
    ctx.moveTo(circle1Left-xOffset+radius, circle1Top-yOffset+radius);
    ctx.lineTo(circle2Left-xOffset+radius, circle2Top-yOffset+radius);
    ctx.lineWidth = 1.5
    ctx.stroke()
    ctx.moveTo(circle2Left-xOffset+radius, circle2Top-yOffset+radius);
    ctx.lineTo(circle2Left-xOffset+radius-headlen*Math.cos(angle-Math.PI/7),circle2Top-yOffset+radius-headlen*Math.sin(angle-Math.PI/7));
    ctx.lineTo(circle2Left-xOffset+radius-headlen*Math.cos(angle+Math.PI/7),circle2Top-yOffset+radius-headlen*Math.sin(angle+Math.PI/7));
    ctx.lineTo(circle2Left-xOffset+radius, circle2Top-yOffset+radius);
    ctx.lineTo(circle2Left-xOffset+radius-headlen*Math.cos(angle-Math.PI/7),circle2Top-yOffset+radius-headlen*Math.sin(angle-Math.PI/7));
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1.5;
    ctx.stroke();
    ctx.fillStyle = "black";
    ctx.fill();
    //addEdgeMat()
}

drawUnDirEdge = (node1x, node1y, node2x, node2y) => {
    let can = document.getElementById("can");
    let bodyRect = document.body.getBoundingClientRect(), canRect = can.getBoundingClientRect()
    let yOffset = canRect.top-bodyRect.top
    let xOffset = canRect.left-bodyRect.left
    document.body.style.padding = "0"
    document.body.style.margin = "0"
    let node1 = (parseInt(node1y) - 1) * 15 + parseInt(node1x)
    let node2 = (parseInt(node2y) - 1) * 15 + parseInt(node2x)
    let Node1 = document.getElementById(`${node1}`)
    let Node2 = document.getElementById(`${node2}`)
    let circle1Left = Node1.getBoundingClientRect().left
    let circle1Top = Node1.getBoundingClientRect().top
    let circle2Left = Node2.getBoundingClientRect().left
    let circle2Top = Node2.getBoundingClientRect().top
    let radius = (Node1.getBoundingClientRect().right - Node1.getBoundingClientRect().left) / 2
    radius = parseFloat(radius)
    let ctx = can.getContext("2d");
    ctx.beginPath();
    ctx.globalCompositeOperation='source-over';

    let x1,y1,x2,y2;

    x1 = circle1Left-xOffset+radius;
    x2 = circle2Left-xOffset+radius;
    y1 = circle1Top-yOffset+radius;
    y2 = circle2Top-yOffset+radius;
    // let slope = (y2 - y1)/(x2-x1)//calculate tan of angle between nodes
    // slope = Math.atan(slope);
    // let cosine = Math.cos(slope);
    // let sin = Math.sin(slope);
    // if(cosine > 0)
    // {
    //     x1 += cosine*radius;
    //     x2 -= cosine*radius;
    //     if(sin > 0)
    //     {
    //         y1 += sin*radius;
    //         y2 -= sin*radius;
    //     }else{
    //         y1 -= sin*radius;
    //         y2 += sin*radius;
    //     }
    
    // }else{
    //     x1 -= cosine*radius;
    //     x2 += cosine*radius;
    //     if(sin > 0)
    //     {
    //         y1 += sin*radius;
    //         y2 -= sin*radius;
    //     }else{
    //         y1 -= sin*radius;
    //         y2 += sin*radius;
    //     }
    // }
    ctx.globalCompositeOperation='destination-over';
    ctx.moveTo(circle1Left-xOffset+radius, circle1Top-yOffset+radius);
    ctx.lineTo(circle2Left-xOffset+radius, circle2Top-yOffset+radius);
    ctx.lineWidth = 1.5
    ctx.strokeStyle = "black"
    ctx.lineWidth = 1.5
    ctx.stroke()
    //addEdgeMat()
}
makeCanvas = () => {
    document.body.innerHTML += `<canvas width="${(0.75 * document.documentElement.clientWidth)}px" height="${(0.75 * document.documentElement.clientHeight)}px" id="can"></canvas>`
    can.style.position = "absolute"
    can.style.top = "45%"
    can.style.left = "10%" }

drawUnDirEdgeForm = () => {
    document.body.innerHTML += `<form name="edgeMaker" onsubmit="drawUnDirEdge(edgeMaker.node1x.value, edgeMaker.node1y.value, edgeMaker.node2x.value, edgeMaker.node2y.value);return false">
    Enter First Node X coordinate:<br>
    <input type="text" name="node1x"><br>
    Enter First Node Y coordinate:<br>
    <input type="text" name="node1y"><br>
    Enter Second Node X coordinate:<br>
    <input type="text" name="node2x"><br>
    Enter Second Node Y coordinate:<br>
    <input type="text" name="node2y">
    <input type="submit">
    </form>`
}

drawDirEdgeForm = () => {
    document.body.innerHTML += `<form name="dirEdgeMaker" onsubmit="drawDirEdge(dirEdgeMaker.node1x.value, dirEdgeMaker.node1y.value, dirEdgeMaker.node2x.value, dirEdgeMaker.node2y.value);return false">
    Enter First Node X coordinate:<br>
    <input type="text" name="node1x"><br>
    Enter First Node Y coordinate:<br>
    <input type="text" name="node1y"><br>
    Enter Second Node X coordinate:<br>
    <input type="text" name="node2x"><br>
    Enter Second Node Y coordinate:<br>
    <input type="text" name="node2y">
    <input type="submit">
    </form>`
}

dirGraphGenerator = () => {

drawGraphForm()
drawDirEdgeForm()

}

unDirGraphGenerator = () => {

    drawGraphForm()
    drawUnDirEdgeForm()
    
}

/* Takes as input x and y coordinates of node, and draws node.*/
makeGraph = (x, y) => {
    let id = (parseInt(y) - 1) * 15 + parseInt(x)
    let graph = document.getElementById(`${id}`)
    let graphVisible = graph.getAttribute("visibility")
    if (graphVisible.localeCompare("hidden") == 0){
        graph.setAttribute("visibility", "visible")
        //addNodeMat(mat, num)    
    }
    else{
        alert("Cannot reselect node!")
    }
    return
}

/* Intialises canvas and node-drawing setup.*/
drawGraphNodes = () => {
    let container = document.createElement('div')
    document.body.appendChild(container)
    container.id = 'container';
    container.style.width = `${document.documentElement.clientWidth *  0.75}px`
    container.style.height = `${document.documentElement.clientHeight * 0.75}px`
    container.style.alignContent='center'
    container.style.position = "absolute"
    container.style.left = "10%"
    container.style.top = "45%"
    let gridNumber = 1
    let xDirection = 15
    let yDirection = 15
    for (let indexY = 0; indexY < yDirection; indexY++) {
        for (let indexX = 0; indexX < xDirection; indexX++) {
            let div = document.createElement('div');
            container.appendChild(div);
            let width = (document.documentElement.clientWidth *  0.75)/ 15
            let height = (document.documentElement.clientHeight *  0.75)/ 15
            console.log(width, height)
            div.style.width = `${width}px`
            div.style.height = `${height}px`
            div.id = `div${gridNumber}`;
            div.style.position= "absolute"          
            div.style.top = `${indexY * height}px`
            div.style.left = `${indexX * width}px`
            div.style.alignContent = `center`
            div.innerHTML = `<svg width="100%" height="100%" id="svg${gridNumber}">
                                <circle cx="50%" cy="50%" visibility="hidden" fill="white" id=${gridNumber++} r="25%" stroke="#000000" stroke-width="3%"/> 
                            </svg>`
        }
    }
    let can = document.getElementById(`can`)
}

drawGraphForm = () => {
    document.body.innerHTML += `<form name="nodeMaker" onsubmit="makeGraph(nodeMaker.x.value, nodeMaker.y.value); return false">
                                   Enter x coordinate:<br>
                                    <input type="text" name="x">
                                    <br>
                                    Enter y coordinate:<br>
                                    <input type="text" name="y">
                                    <input type="submit">
                                </form>`
}
