/* Takes as input adjacency matrix and number of nodes
Checks if there is a cycle at any vertex. */
isDAG = (adjMtx, num) => {
    let visited = [];
    let recStack = [];
    for (let i = 0; i < num; i++) 
    {
        visited[i] = false; 
        recStack[i] = false;
    }
    for (let i = 0; i < num; i++){
        if(detectCycle(i, adjMtx, num, visited, recStack))
            return false;
    return true;
    }
}

/* Takes as input, the vertex, object of type Graph, visted and recStack
 Detects cycle starting from a given node. */

detectCycle = (vert, adjMtx, num, visited, recStack) => { 
    if(visited[vert] == false)
    {
        visited[vert] = true;
        recStack[vert] = true;

        for(var i = 0; i < num; i++){
            let element = adjMtx[vert][i];
            if(element == 1)
            {
                element = i;
                if(visited[element] == false && detectCycle(element, adjMtx, num, visited, recStack))
                    return true;
                else if(recStack[element] == true)
                    return true;
            }
        }
    }
    recStack[vert] = false;
    return false; 
}