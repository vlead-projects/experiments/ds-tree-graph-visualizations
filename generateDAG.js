/* Takes as input minimum and maximum 'height'(depth of DAG) and 'width'(max nodes at same level). Draws DAG */
generateDAG = (nodesMin, nodesMax, heightMin, heightMax) => {
    var adjMtx = [];
    
   let percent = 30;

    var nodes = 0;
    var ranks = Math.floor(parseInt(heightMin)
                + (Math.random () * (parseInt(heightMax) - parseInt(heightMin) + 1)));
  
   for(var i = 0; i<ranks*4; i++)
    {
      adjMtx[i] = [];
      for(var j = 0; j<ranks*4; j++)
        adjMtx[i][j] = 0;
    }
    for (var i = 0; i < ranks; i++)
      {
        var newNodes = Math.floor(parseInt(nodesMin)
                        + (Math.random () * (parseInt(nodesMax) - parseInt(nodesMin) + 1)));
  
        for (var j = 0; j < nodes; j++)
          for (var k = 0; k < newNodes; k++)
            if ( (Math.random () * 100) < percent)
                adjMtx[j][k + nodes] = 1;
        
        nodes += newNodes;  
    } 
    console.log(adjMtx, ranks*4)
    return [adjMtx, ranks*4];
}

/* Takes as input minimum and maximum 'height'(depth of DAG, max value of 3) and 'width'(max nodes at same level, max value of 3). 
Draws DAG */

DAGGenerator = (nodesMin, nodesMax, heightMin, heightMax) => {
  console.log("test print")
  let outputMat = generateDAG(nodesMin, nodesMax, heightMin, heightMax)
  let num = outputMat[1]
  let coord = randomGeneratorNode(num)
   for ( let i = 0; i < num; i++){
    for (let j = 0; j< num; j++){
      if(outputMat[0][i][j] == 1){
        drawDirEdge(coord[0][i], coord[1][i], coord[0][j], coord[1][j])
      }
    }
  } 
}

/* Utility function to call a form to pass required arguments to DAG Generator function. */

DAGForm = () => {

document.body.innerHTML += `<form name="DAGMaker" onsubmit="DAGGenerator(DAGMaker.widthMin.value, DAGMaker.widthMax.value, DAGMaker.heightMin.value, DAGMaker.heightMax.value); return false">
  Enter Min Width of DAG:<br>
  <input type="text" name="widthMin"><br>
  Enter Max Width of DAG:<br>
  <input type="text" name="widthMax"><br>
  Enter Min Height of DAG:<br>
  <input type="text" name="heightMin"><br>
  Enter Max Height of DAG:<br>
  <input type="text" name="heightMax">
  <input type="submit">
  </form>`
}